package com.test;

 

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Example {
	static prime obj;
	@BeforeAll
	public static void abc1()
	 {
		obj = new prime();
		 System.out.println("I am BeforeAll");
	 }
	
	 @BeforeEach
	 public   void abc2() {
		 
		 System.out.println("I am BeforeEach");
	 }
	 @AfterAll
	 public  static  void abc3()
	 {
		 System.out.println("I am AfterAll");
	 }
	  
	 
	@AfterEach
	 public   void abc4()
	 {
		 System.out.println("I am AfterEach");
	 }
	 @Test
	 public void abc5()
	 {
		 assertEquals(obj.Armstrong(1),true);
		 System.out.println("I am TestCase");
	 }
	 @Test
	 public void abc6()
	 {
		 assertEquals(obj.Armstrong(153),true);
		 System.out.println("I am TestCase");
	 }
	 
}
