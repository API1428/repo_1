package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class dbms {

	public static void main(String []args)
	{
		try {
			//import the jdbc driver
			Class.forName("com.mysql.cj.jdbc.Driver");	//depreciated......
			//Driver Manager = store the server path
			//Connection = pass the driver manager to the jdbc driver. => make a connection
			Connection a = DriverManager.getConnection("jdbc:mysql://localhost:3306/invoice","root","");
			//Statement  = pass the query => query executed => response
			Statement b = a.createStatement();
			
			ResultSet c = b.executeQuery("select * from invoicemanagementsystem");
		
			//Result Set = Used to store the response
			//Iteration Method = We will access the response and display them on the console.
			while(c.next())
			{
				System.out.println(c.getString(1) +" "+ c.getString(2) + " " +c.getString(3));
			}
			
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
	}
}
